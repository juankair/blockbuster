<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->uuid('id');
            $table->char('title',100);
            $table->char('thumbnail',225);
            $table->longText('synopsis');
            $table->date('release_date');
            $table->uuid('genre');
            $table->char('youtube',100);
            $table->char('director',75);
            $table->char('rating',5);
            $table->char('run_time',5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}

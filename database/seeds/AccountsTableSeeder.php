<?php

require 'vendor/autoload.php';

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Ramsey\Uuid\Uuid;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create('id_ID');

      \App\Model\Account::create([
        'id' => Uuid::uuid4()->toString(),
        'fullname' => $faker->name,
        'username' => 'admin',
        'password' => bcrypt('admin'),
        'email' => $faker->unique()->safeEmail,
        'notelp' => $faker->unique()->phoneNumber,
        'active' => 1,
      ]);
    }
}

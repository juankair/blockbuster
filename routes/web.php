<?php


// ==============================================================================
// FRONT END
// ==============================================================================

// HOMEPAGE | GET
Route::get('/','Frontend\MainController@homepage');

// FILM | GET
Route::get('/listfilm','Frontend\MainController@listFilm');
Route::get('/allfilm','Frontend\MainController@allFilm');
Route::get('/detail/{id}','Frontend\MainController@detailFilm');

// FILM | POST
Route::post('/submitReview','Frontend\MainController@submitReview');





// ==============================================================================
// BACK END
// ==============================================================================

// LOGIN | GET
Route::get('/admin/login','Auth\LoginController@index');
// Login | POST
Route::post('/admin/login/check','Auth\LoginController@checkLogin');

// LOGOUT | GET
Route::get('/admin/logout','Auth\LoginController@logout');


// DASHBOARD | GET
Route::get('/admin','Backend\MainController@dashboard');




// FILM | GET
Route::get('/admin/film','FilmController@listFilm');
Route::get('/admin/film/add','FilmController@formFilm');
Route::get('/admin/film/edit/{id}','FilmController@editFilm');
// FILM | POST
Route::post('/admin/film/save','FilmController@saveFilm');
Route::post('/admin/film/edit','FilmController@saveEditFilm');
Route::post('/admin/film/delete','FilmController@deleteFilm');



// ACCOUNT | GET
Route::get('/admin/master/account','AccountController@listAccount');
// ACCOUNT | POST
Route::post('/admin/master/account/save','AccountController@saveAccount');
Route::post('/admin/master/account/edit','AccountController@editAccount');
Route::post('/admin/master/account/changeStatus','AccountController@statusAccount');
Route::post('/admin/master/account/changePassword','AccountController@passwordAccount');

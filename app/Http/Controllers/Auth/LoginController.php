<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Model\Account;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
      return view('backend/auth/login');
    }

    public function logout(){
        Session::flush();
        return redirect('admin/login');
    }


      public function checkLogin(Request $request){
        $auth = false;
        $check = Account::where('username',$request->input('username'))->first();

        if (!empty($check)) {
          if (Hash::check($request->password, $check->password)) {
            session([
              'fullname' => $check->fullname,
              'account_id' => $check->id,
            ]);
            $auth = true;
          }
        }

        if ($auth) {
            echo json_encode(array('status' => TRUE,'fullname'=>$check->fullname));
        }else{
            echo json_encode(array('status' => FALSE));
        }
      }
}

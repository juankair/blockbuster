<?php

namespace App\Http\Controllers\Frontend;

require 'vendor/autoload.php';

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Film;
use App\Model\Review;
use Ramsey\Uuid\Uuid;


class MainController extends Controller
{
  public function homepage(){
    $data['dataFilm'] = Film::get();
    return view('frontend/dashboard',$data);
  }


  public function allFilm(){
    $sort = (!empty($_GET['sort']) ? $_GET['sort'] : 'desc');
    $keywords = (!empty($_GET['keywords']) ? $_GET['keywords'] : null);
    $genre = (!empty($_GET['genre']) ? $_GET['genre'] : null);
    $rating = (!empty($_GET['rating']) ? $_GET['rating'] : null);
    $year_from = (!empty($_GET['year_from']) ? $_GET['year_from'] : null);
    $year_to = (!empty($_GET['year_to']) ? $_GET['year_to'] : date('Y'));


    $data['dataFilm'] = Film::where([
      ['title','like','%'.$keywords.'%'],
      ['genre','like','%'.$genre.'%'],
      ['rating','like','%'.$rating.'%'],
      ])->
      whereBetween('release_date',[$year_from.'-01-1',$year_to.'-12-31'])->
      orderBy('release_date',$sort)->
      get();

    return view('frontend/film/allFilm',$data);
  }


  public function detailFilm($id){
    $data['dataFilm'] = Film::where('id',$id)->first();
    $data['reviews'] = Review::where('film_id',$id)->get();
    return view('frontend/film/detail',$data);
  }


  public function submitReview(Request $request){
    Review::create([
      'id' => Uuid::uuid4()->toString(),
      'film_id' => $request->film_id,
      'rating' => 0,
      'name' => $request->name,
      'message' => $request->message,
    ]);
  }
}

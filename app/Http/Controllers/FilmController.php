<?php

namespace App\Http\Controllers;

require 'vendor/autoload.php';

use Illuminate\Http\Request;
use App\Model\Film;
use Ramsey\Uuid\Uuid;

class FilmController extends Controller
{


  protected $redirectTo = '/admin/login';



  public function __construct(){
    $this->middleware('AuthRules');
  }


  public function listFilm(){
    $data['listFilm'] = Film::get();
    return view('backend/mainMenu/film/listFilm',$data);
  }



  public function formFilm($id=null){
      $data['dataFilm'] = [];
      return view('backend/mainMenu/film/formFilm',$data);
  }



  public function saveFilm(Request $request){
    $filename = null;

    if (!empty($request->file('thumbnail'))) {
				$file = $request->file('thumbnail');
				$ext = $file->getClientOriginalExtension();
				if (strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg' || strtolower($ext) == 'png') {
					$tujuan_upload = 'public/uploads/thumbnail';
          $name = Uuid::uuid4()->toString();
					$filename = $name.'.'.$file->getClientOriginalExtension();
					$file->move($tujuan_upload,$filename);
				}
			}

      Film::create([
        'id' => Uuid::uuid4()->toString(),
        'title' => $request->title,
        'thumbnail' => $filename,
        'synopsis' => $request->synopsis,
        'release_date' => date('Y-m-d',strtotime($request->release_date)),
        'genre' => $request->genre,
        'youtube' => $request->trailer,
        'director' => $request->director,
        'rating' => $request->rating,
        'run_time' => $request->run_time,
      ]);

      return redirect('admin/film');
  }



  public function saveEditFilm(Request $request){

    $original_data = Film::where('id',$request->film_id)->first();
    $filename = $original_data->thumbnail;

    if (!empty($request->file('thumbnail'))) {
        $path = public_path()."/uploads/thumbnail/".$original_data->thumbnail;
          if (file_exists($path)) {
            unlink($path);
          }

				$file = $request->file('thumbnail');
				$ext = $file->getClientOriginalExtension();
				if (strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg' || strtolower($ext) == 'png') {
					$tujuan_upload = 'public/uploads/thumbnail';
          $name = Uuid::uuid4()->toString();
					$filename = $name.'.'.$file->getClientOriginalExtension();
					$file->move($tujuan_upload,$filename);

				}
			}

      Film::where('id',$request->film_id)->update([
        'title' => $request->title,
        'thumbnail' => $filename,
        'synopsis' => $request->synopsis,
        'release_date' => date('Y-m-d',strtotime($request->release_date)),
        'genre' => $request->genre,
        'youtube' => $request->trailer,
        'director' => $request->director,
        'rating' => $request->rating,
        'run_time' => $request->run_time,
      ]);

      return redirect('admin/film');
  }




  public function editFilm($id){
    $data['dataFilm'] = Film::where('id',$id)->first();
    if (!empty($data['dataFilm'])) {
      return view('backend/mainMenu/film/formFilm',$data);
    }else{
      return redirect('admin/film');
    }
  }


  public function deleteFilm(Request $request){
$original_data = Film::where('id',$request->id)->first();

    $path = public_path()."/uploads/thumbnail/".$original_data->thumbnail;
      if (file_exists($path)) {
        unlink($path);
      }

      Film::where('id',$request->id)->delete();
  }


}

<?php
namespace App\Http\Controllers;

require 'vendor/autoload.php';

use Illuminate\Http\Request;
use App\Model\Account;
use Ramsey\Uuid\Uuid;

class AccountController extends Controller
{
  public function __construct(){
    $this->middleware('AuthRules');
  }

  public function listAccount(){
    $data['dataAccount'] = Account::get();
    return view('backend/master/account/listAccount',$data);
  }

  public function saveAccount(Request $request){
    Account::create([
      'id' => Uuid::uuid4()->toString(),
      'fullname' => $request->fullname,
      'username' => $request->username,
      'password' => bcrypt($request->password),
      'email' => $request->email,
      'notelp' => $request->notelp,
      'active' => 1,
    ]);
  }

  public function editAccount(Request $request){
    Account::where('id',$request->account_id)->update([
      'fullname' => $request->fullname,
      'username' => $request->username,
      'email' => $request->email,
      'notelp' => $request->notelp,
    ]);
  }

  public function passwordAccount(Request $request){
    Account::where('id',$request->ac_id)->update([
      'password' => bcrypt($request->new_password)
    ]);
  }

  public function statusAccount(Request $request){
    $data = Account::where('id',$request->id)->first();
    if (!empty($data)) {
      $status = ($data->active == '1' ? 0 : 1);
      Account::where('id',$request->id)->update([
        'active' => $status
      ]);
    }
  }

}

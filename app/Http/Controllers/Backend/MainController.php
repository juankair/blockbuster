<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
  protected $redirectTo = '/admin/login';

  public function __construct(){
    $this->middleware('AuthRules');
  }

  public function dashboard(){
    return view('backend/mainMenu/dashboard');
  }

}

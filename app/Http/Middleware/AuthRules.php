<?php

namespace App\Http\Middleware;

use Illuminate\Routing\Redirector;
use Closure;
use Session;

class AuthRules
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      if (!empty(session('account_id'))) {
        return $next($request);
      }else{
        return redirect('admin/login');
      }
    }
}

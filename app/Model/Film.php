<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{

  protected $guarded = [];

  public $timestamps = true;

  protected $primaryKey = 'id';

  protected $keyType = 'string';

  public $incrementing = false;
}

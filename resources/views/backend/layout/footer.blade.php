            <footer class="footer text-center text-sm-left">&copy; 2019 Lambda Sangkala M | Test Hamura Agency
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->
    <!-- jQuery  -->
    <script src="{{asset('admin/assets/')}}/js/jquery.min.js"></script>
    <script src="{{asset('admin/assets/')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('admin/assets/')}}/js/metisMenu.min.js"></script>
    <script src="{{asset('admin/assets/')}}/js/waves.min.js"></script>
    <script src="{{asset('admin/assets/')}}/js/jquery.slimscroll.min.js"></script>
    <script src="{{asset('admin/assets/')}}/plugins/apexcharts/apexcharts.min.js"></script>
    <script src="{{asset('admin/assets/')}}/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="{{asset('admin/assets/')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('admin/assets/')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('admin/assets/')}}/pages/jquery.hospital_dashboard.init.js"></script>
    <!-- App js -->
    <script src="{{asset('admin/assets/')}}/js/app.js"></script>

</body>

</html>

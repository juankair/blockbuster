<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>BlockBuster - Lambda Sangkala - Tes Hamura</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta charset="UTF-8">
    <meta name="description" content="Tes Hamura">
    <meta name="author" content="Lambda Sangkala Murbawisesa">
    <link rel="profile" href="#">
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/')}}/images/logo.png">
    <link href="{{asset('admin/assets/')}}/plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
    <!-- DataTables -->
    <link href="{{asset('admin/assets/')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <!-- Responsive datatable examples -->
    <link href="{{asset('admin/assets/')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <!-- App css -->
    <link href="{{asset('admin/assets/')}}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/')}}/css/icons.css" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/')}}/css/metisMenu.min.css" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/assets/')}}/css/style.css" rel="stylesheet" type="text/css">
</head>

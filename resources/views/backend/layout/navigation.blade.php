
<body>
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{url('')}}" class="logo"> <span>
            <img src="{{ asset('assets') }}/images/logo2.png" alt="logo-small" >
            </span>
            </a>
        </div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom">

            <ul class="list-unstyled topbar-nav float-right mb-0">



                <li class="dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <!-- <img src="{{asset('admin/assets_min/')}}/images/users/user-4.jpg" alt="profile-user" class="rounded-circle">  -->
                        <span class="ml-1 nav-user-name hidden-sm">{{session('fullname')}}
                                                                                                                        <i class="mdi mdi-chevron-down"></i>
                                                                                                                    </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- <a class="dropdown-item" href="#">  <i class="dripicons-user text-muted mr-2"></i> Profile</a>
                        <a class="dropdown-item" href="#">  <i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a>
                        <a class="dropdown-item" href="#">  <i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                        <a class="dropdown-item" href="#">  <i class="dripicons-lock text-muted mr-2"></i> Lock screen</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="{{ url('admin/logout') }}">  <i class="dripicons-exit text-muted mr-2"></i> Logout</a>
                    </div>
                </li>


            </ul>
            <!--end topbar-nav-->
            <ul class="list-unstyled topbar-nav mb-0">
                <li>
                    <button class="button-menu-mobile nav-link waves-effect waves-light">   <i class="dripicons-menu nav-icon"></i>
                    </button>
                </li>


            </ul>
        </nav>
        <!-- end navbar-->
    </div>
    <!-- Top Bar End -->
    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <div class="left-sidenav">
            <div class="main-icon-menu">
                <nav class="nav">

                    <a href="#MetricaHospital" class="nav-link nav_mainmenu autoclick" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Main Menu">
                      <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                          <path class="svg-primary" d="M208 448V320h96v128h97.6V256H464L256 64 48 256h62.4v192z"/>
                      </svg>
                    </a>


                    <a href="#master_data" class="nav-link nav_master" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Master Data">
                      <svg class="nav-svg" version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                          <path class="svg-primary" d="M449.2 208H423v-32l-14.4-48H383V96l-15-48H144l-15 48v32h-25.6L89 176v32H62.8L48 256v165.3c0 23.5 35.2 42.7 58.7 42.7h314.7c21.8 0 42.7-19.7 42.7-41V256l-14.9-48zM176 96h160v32H176V96zm-41 80h242v32H135v-32zm282 112h-82.6c-7.4 36.5-39.7 64-78.4 64s-71-27.5-78.4-64H95v-32h322v32z"/>
                      </svg>
                    </a>







                    <!--end MetricaOthers-->

                    <!--end MetricaAuthentication-->
                </nav>
                <!--end nav-->
            </div>
            <!--end main-icon-menu-->
            <div class="main-menu-inner">
                <div class="menu-body slimscroll">
                    <div id="MetricaHospital" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Main Menu</h6>
                        </div>
                        <ul class="nav metismenu">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/admin')}}"> Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/admin/film')}}"> Film</a>
                            </li>
                            <!--end nav-item-->
                        </ul>
                        <!--end nav-->
                    </div>


                    <div id="master_data" class="main-icon-menu-pane">

                        <div class="title-box">
                            <h6 class="menu-title">Master  Data</h6>
                        </div>
                        <ul class="nav metismenu">
                          <li class="nav-item">
                            <a class="nav-link" href="{{url('admin/master/account')}}">Admin Account</a>
                          </li>
                        </ul>
                        
                    </div>
                    <!-- end Authentication-->
                </div>
                <!--end menu-body-->
            </div>
            <!-- end main-menu-inner-->
        </div>
        <!-- end left-sidenav-->

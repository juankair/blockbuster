<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
			<title>BlockBuster - Lambda Sangkala - Tes Hamura</title>
			<meta name="viewport" content="width=device-width,initial-scale=1">
			<meta content="Test Hamura" name="description">
				<meta content="Lambda Sangkala Murbawisesa" name="author">
						<!-- App favicon -->
				        <link rel="shortcut icon" href="{{asset('assets/')}}/images/logo.png">
							<!-- App css -->
							<link href="{{asset('admin/assets/')}}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
								<link href="{{asset('admin/assets/')}}/css/icons.css" rel="stylesheet" type="text/css">
									<link href="{{asset('admin/assets/')}}/css/metisMenu.min.css" rel="stylesheet" type="text/css">
										<link href="{{asset('admin/assets/')}}/css/style.css" rel="stylesheet" type="text/css">
										</head>
										<body class="account-body accountbg">
											<!-- Log In page -->
											<div class="row vh-100">
												<div class="col-lg-12 pr-0 align-self-center">
													<div class="row">
														<div class="col-lg-3 mx-auto">
															<div class="card auth-card shadow-lg">
																<div class="card-body">
																	<div class="px-3">
																		<div class="auth-logo-box">
																			<a href="{{url('/')}}" class="logo logo-admin">
																				<img src="{{asset('assets/')}}/images/logo.png" height="55" alt="logo" class="auth-logo">
																				</a>
																			</div>
																			<!--end auth-logo-box-->
																			<div class="text-center auth-logo-text">
																				<h4 class="mt-0 mb-3 mt-5">Login</h4>
																				<p class="text-muted mb-0">Welcome To Admin Panel BlockBuster</p>
																			</div>
																			<!--end auth-logo-text-->
																			<form id="falogin" class="form-horizontal auth-form my-4" action="{{ url('admin/login/check') }}" method="POST">
																				{{ csrf_field() }}
																				<div class="form-group">
																					<label for="username">Username</label>
																					<div class="input-group mb-3">
																						<span class="auth-form-icon">
																							<i class="dripicons-user"></i>
																						</span>
																						<input type="text" class="form-control" name="username" id="username" placeholder="Enter Username">
																						</div>
																					</div>
																					<!--end form-group-->
																					<div class="form-group">
																						<label for="userpassword">Password</label>
																						<div class="input-group mb-3">
																							<span class="auth-form-icon">
																								<i class="dripicons-lock"></i>
																							</span>
																							<input type="password" class="form-control" name="password" id="userpassword" placeholder="Enter Password">
																							</div>
																						</div>
																						<!--end form-group-->
																						<div class="form-group row mt-4">
																							<div class="col-sm-12">
																								<div class="custom-control custom-switch switch-success">
																									<input type="checkbox" class="custom-control-input" id="customSwitchSuccess">
																										<label class="custom-control-label text-muted" for="customSwitchSuccess">Remember Me</label>
																									</div>
																								</div>
																								<!--end col-->

																								<!--end col-->
																							</div>
																							<!--end form-group-->
																							<div class="form-group mb-0 row">
																								<div class="col-12 mt-2">
																									<button class="btn btn-primary btn-submit btn-round btn-block waves-effect waves-light" type="submit">Login
																										<i class="fas fa-sign-in-alt ml-1"></i>
																									</button>
																								</div>
																								<!--end col-->
																							</div>
																							<!--end form-group-->
                                            </form>
																						<!--end form-->
																					</div>

																				</div>
																				<!--end card-body-->
																			</div>
																			<!--end card-->

																		</div>
																		<!--end col-->
																	</div>
																	<!--end row-->
																</div>
																<!--end col-->
															</div>
															<!--end row-->
															<!-- End Log In page -->
															<!-- jQuery  -->
															<script src="{{asset('admin/assets/')}}/js/jquery.min.js"></script>
															<script src="{{asset('admin/assets/')}}/js/bootstrap.bundle.min.js"></script>
															<script src="{{asset('admin/assets/')}}/js/metisMenu.min.js"></script>
															<script src="{{asset('admin/assets/')}}/js/waves.min.js"></script>
															<script src="{{asset('admin/assets/')}}/js/jquery.slimscroll.min.js"></script>
															<!-- App js -->
															<script src="{{asset('admin/assets/')}}/js/app.js"></script>
															<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

$(document).on('submit','#falogin',function(e){
	e.preventDefault();

	$.post($('#falogin').attr('action'),$('#falogin').serializeArray(),function(data){
		if (JSON.parse(data).status) {
			swal('Success','Welcome '+JSON.parse(data).fullname,'success').then(location.href='{{ url("/admin") }}');
		}else{
			swal('Failed','Incorrect username or password','error');
		}
	});

});

</script>

														</body>

													</html>

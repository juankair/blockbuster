@include('backend/layout/header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@include('backend/layout/navigation')
        <!-- Page Content-->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Management Data Film</h4>
                            <span>{{ (!empty($dataFilm) ? 'Edit Data Film' : 'Add Data Film') }}</span>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <!--end row-->

                <div class="row">
                  <div class="col-md-12">
                    <form class="form_film" action="{{ (!empty($dataFilm) ? url('admin/film/edit') : url('admin/film/save')) }}" enctype="multipart/form-data" method="post">


                    <div class="card card-body">
                      <div class="row">
                        <div class="col-md-12">
                            {{csrf_field()}}
<input type="hidden" name="film_id" value="{{ (!empty($dataFilm->id) ? $dataFilm->id : null) }}">
                            <h4>Basic Data</h4>
                            <br>

                            <div class="form-group">
                              <div class="row">
                                <div class="col-md-6">
                                  <label for="">Title Film</label>
                                  <input type="text" placeholder="Title Film" class="form-control" name="title" value="{{ (!empty($dataFilm->title) ? $dataFilm->title : null) }}">
                                </div>
                                <div class="col-md-3">
                                  <label for="">Director</label>
                                  <input type="text" placeholder="Director" class="form-control" name="director" value="{{ (!empty($dataFilm->director) ? $dataFilm->director : null) }}">
                                </div>
                                <div class="col-md-3">
                                  <label for="">Genre</label>
                                  <select class="form-control" name="genre">
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Science Fiction' ? 'selected="true"' : 'false') : null) }}>Science Fiction</option>
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Musical Drama' ? 'selected="true"' : 'false') : null) }}>Musical Drama</option>
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Drama' ? 'selected="true"' : 'false') : null) }}>Drama</option>
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Thriller' ? 'selected="true"' : 'false') : null) }}>Thriller</option>
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Romantic' ? 'selected="true"' : 'false') : null) }}>Romantic</option>
                                    <option {{ (!empty($dataFilm->genre) ? ($dataFilm->genre == 'Comedy' ? 'selected="true"' : 'false') : null) }}>Comedy</option>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="row">
                                <div class="col-md-4">
                                  <label for="">Rating</label>
                                  <select class="form-control" name="rating">
                                    <option {{ (!empty($dataFilm->rating) ? ($dataFilm->rating == 'G' ? 'selected="true"' : 'false') : null) }}>G</option>
                                    <option {{ (!empty($dataFilm->rating) ? ($dataFilm->rating == 'PG' ? 'selected="true"' : 'false') : null) }}>PG</option>
                                    <option {{ (!empty($dataFilm->rating) ? ($dataFilm->rating == 'PG-13' ? 'selected="true"' : 'false') : null) }}>PG-13</option>
                                    <option {{ (!empty($dataFilm->rating) ? ($dataFilm->rating == 'R' ? 'selected="true"' : 'false') : null) }}>R</option>
                                    <option {{ (!empty($dataFilm->rating) ? ($dataFilm->rating == 'NC-17' ? 'selected="true"' : 'false') : null) }}>NC-17</option>
                                  </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="">Release Date</label>
                                  <input type="text" placeholder="Release Date" class="form-control dp" name="release_date" value="{{ (!empty($dataFilm->release_date) ? date('m/d/Y',strtotime($dataFilm->release_date)) : null) }}">
                                </div>
                                <div class="col-md-4">
                                  <label for="">Run Time (Minute)</label>
                                  <input type="text" placeholder="Run Time (Minute)" class="form-control" name="run_time" value="{{ (!empty($dataFilm->run_time) ? $dataFilm->run_time : null) }}">
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="row">
                                <div class="col-md-12">
                                  <label for="">Synopsis</label>
                                  <textarea name="synopsis" class="form-control" placeholder="Synopsis" rows="8" cols="80">{{ (!empty($dataFilm->synopsis) ? $dataFilm->synopsis : null) }}</textarea>
                                </div>
                              </div>
                            </div>


                            <h4>Media</h4>
                            <br>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-md-6">
                                  <label for="">Thumbnail Poster</label>
                                  <input type="file" class="form-control" name="thumbnail" value="">
                                </div>
                                <div class="col-md-6">
                                  <label for="">Trailer</label>
                                  <input type="text" class="form-control" placeholder="Trailer From Youtube (Link)" name="trailer" value="{{ (!empty($dataFilm->youtube) ? $dataFilm->youtube : null) }}">
                                </div>
                              </div>
                            </div>


                        </div>
                      </div>
                    </div>
                    <br>
                    <center>
                      <button type="button" class="btn btn-success btn-save-data" name="button">
                        Save Data
                      </button>

                      <a href="{{url('admin/film')}}">
                        <button type="button" class="btn btn-default" name="button">
                          Back
                        </button>
                      </a>
                    </center>

                  </form>
                  </div>
                </div>



            </div>
            <!-- container -->

@include('backend/layout/footer')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>

$('.nav_mainmenu').trigger('click');

  $('.dp').datepicker();

  $(document).on('click','.btn-save-data',function(e){
    e.preventDefault();

    swal({
      title: "Confirmation",
      text: "Do you really want to submit the form?",
      icon: "info",
      buttons: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('.form_film').submit();
      }
    });
  });

</script>

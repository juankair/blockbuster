@include('backend/layout/header')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@include('backend/layout/navigation')
        <!-- Page Content-->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Management Data Film</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <!--end row-->

                <div class="row">
                  <div class="col-md-12">
                    <div class="card card-body">
                      <div class="row">
                        <div class="col-md-12">
                          <a href="{{ url('admin/film/add') }}">
                            <button type="button" class="btn btn-sm btn-success" name="button">Create New Film</button>
                          </a>

                          <table style="margin-top:20px" class="table table-stripped">
                            <thead>
                              <tr>
                                <th>Title</th>
                                <th>Director</th>
                                <th>Genre</th>
                                <th>Release Date</th>
                                <th>Run Time</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($listFilm)) {
                                ?>
                                  @foreach($listFilm as $value)
                                    <tr>
                                      <td>{{$value->title}}</td>
                                      <td>{{$value->director}}</td>
                                      <td>{{$value->genre}}</td>
                                      <td>{{date('d M Y',strtotime($value->release_date))}}</td>
                                      <td>{{$value->run_time}} minutes</td>
                                      <th>
                                        <a href="{{ url('admin/film/edit').'/'.$value->id }}">
                                          <button type="button" class="btn btn-sm btn-success" name="button">Edit</button>
                                        </a>

                                        <button type="button" class="btn btn-sm btn-danger btn-hapus" data="{{$value->id}}" name="button">Delete</button>
                                      </th>
                                    </tr>
                                  @endforeach
                                <?php
                              }else{
                                ?>
                                <tr>
                                  <td colspan="6">
                                    <center>
                                      film data has not been inputted
                                    </center>
                                  </td>
                                </tr>
                                <?php
                              }
                               ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>



            </div>
            <!-- container -->

@include('backend/layout/footer')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>


  $(document).on('click','.btn-hapus',function(e){
    e.preventDefault();

    swal({
      title: "Confirmation",
      text: "Are you sure to delete this film?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.post('{{ url("admin/film/delete") }}',{"_token": "{{ csrf_token() }}","id" : $(this).attr('data')},function(data){
          swal('Success','Film has been deleted','success').then(location.reload());
        });
      }
    });
  });


</script>

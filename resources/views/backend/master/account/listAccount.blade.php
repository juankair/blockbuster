@include('backend/layout/header')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@include('backend/layout/navigation')
        <!-- Page Content-->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Management Data Account</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <!--end row-->

                <div class="row">
                  <div class="col-md-12">
                    <div class="card card-body">
                      <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-sm btn-success btn-add-customer" name="button">Create New Account</button>

                          <table style="margin-top:20px" class="table table-stripped">
                            <thead>
                              <tr>
                                <th>Username</th>
                                <th>Fullname</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Status</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($dataAccount)) {
                                ?>
                                @foreach($dataAccount as $value)
                                  <tr>
                                    <td>{{$value->username}}</td>
                                    <td>{{$value->fullname}}</td>
                                    <td>{{$value->email}}</td>
                                    <td>{{$value->notelp}}</td>
                                    <td>
                                      <button type="button" class="btn btn-sm btn-{{ ($value->active == '1' ? 'success' : 'danger') }} btn-status" data="{{$value->id}}" name="button">
                                        {{ ($value->active == '1' ? 'Active' : 'Non-Active') }}
                                      </button>
                                    </td>
                                    <td>
                                      <button type="button" data="{{ json_encode($value) }}" class="btn btn-sm btn-success btn-show-edit" name="button">Edit</button>
                                      <button type="button" data="{{$value->id}}" class="btn btn-sm btn-success btn-change-password" name="button">Change Password</button>
                                    </td>
                                  </tr>
                                @endforeach
                                <?php
                              }else{
                                ?>
                                <tr>
                                  <td colspan="6">
                                    <center>
                                      data has not been inputted
                                    </center>
                                  </td>
                                </tr>
                                <?php
                              }
                               ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>



            </div>
            <!-- container -->


            <div class="modal_account modal" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="fnewaccount" action="" method="post">
                      {{@csrf_field()}}
                      <input type="hidden" name="account_id" value="">

                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-12">
                            <label for="">Fullname</label>
                            <input type="text" placeholder="Fullname" class="form-control" name="fullname" value="">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          <div class="col_username col-md-6">
                            <label for="">Username</label>
                            <input type="text" placeholder="Username" class="form-control" name="username" value="">
                          </div>
                          <div class="col_password col-md-6" hidden="false">
                            <label for="">Password</label>
                            <input type="password" placeholder="Password" class="form-control" name="password" value="">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="">Email</label>
                            <input type="email" placeholder="Email" class="form-control" name="email" value="">
                          </div>
                          <div class="col-md-6">
                            <label for="">Phone Number</label>
                            <input type="text" placeholder="Phone Number" class="form-control" name="notelp" value="">
                          </div>
                        </div>
                      </div>


                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-submit-account">Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>


            <div class="modal_password modal" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="fchangepassword" action="{{ url('admin/master/account/changePassword') }}" method="post">
                      {{csrf_field()}}
                      <input type="hidden" name="ac_id" value="">
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-12">
                            <label for="">New Password</label>
                            <input type="password" placeholder="New Password" class="form-control" name="new_password" value="">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-submit-password">Change Password</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

@include('backend/layout/footer')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>

  $(document).on('click','.btn-add-customer',function(e){
    e.preventDefault();

    $('.fnewaccount').attr('action','{{ url("admin/master/account/save") }}');
    $('.modal-title').text('Create New Account');
    $('.btn-submit-account').text('Submit Account');
    $('.modal_account').modal('show');

    $('.col_username').removeClass('col-md-12');
    $('.col_username').addClass('col-md-6');

    $('.col_password').attr('hidden',false);
  });


  $(document).on('click','.btn-show-edit',function(e){
    e.preventDefault();

    $('.fnewaccount').attr('action','{{ url("admin/master/account/edit") }}');
    $('.modal-title').text('Edit Data Account');
    $('.btn-submit-account').text('Save changes');
    $('.modal_account').modal('show');

    let data = JSON.parse($(this).attr('data'));

    $('.col_username').removeClass('col-md-6');
    $('.col_username').addClass('col-md-12');

    $('.col_password').attr('hidden',true);

    $('[name="account_id"]').val(data.id);
    $('[name="fullname"]').val(data.fullname);
    $('[name="username"]').val(data.username);
    $('[name="email"]').val(data.email);
    $('[name="notelp"]').val(data.notelp);
  });


  $(document).on('click','.btn-submit-account',function(){
      swal({
        title: "Confirmation",
        text: "Do you really want to submit the form?",
        icon: "info",
        buttons: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.post($('.fnewaccount').attr('action'),$('.fnewaccount').serializeArray(),function(data){
            swal('Success','Data Saved Successfully','success').then(location.reload());
          });
        }
      });
  });

  $(document).on('click','.btn-status',function(e){
    e.preventDefault();

    swal({
      title: "Confirmation",
      text: "Do you really want to chnage the status for this account?",
      icon: "info",
      buttons: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.post('{{ url("admin/master/account/changeStatus") }}',{"_token": "{{ csrf_token() }}","id" : $(this).attr('data') },function(data){
          swal('Success','Data Saved Successfully','success').then(location.reload());
        });
      }
    });
  });

  $(document).on('click','.btn-change-password',function(e){
    e.preventDefault();

    $('[name="ac_id"]').val($(this).attr('data'));
    $('.modal_password').modal('show');
  });

  $(document).on('click','.btn-submit-password',function(e){
    e.preventDefault();

    swal({
      title: "Confirmation",
      text: "Do you really want to chnage the password for this account?",
      icon: "info",
      buttons: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.post($('.fchangepassword').attr('action'),$('.fchangepassword').serializeArray(),function(data){
          swal('Success','Data Saved Successfully','success').then(location.reload());
        });
      }
    });
  });


</script>

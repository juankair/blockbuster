@include('frontend/layout/header')

<div class="hero mv-single-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- <h1> movie listing - list</h1>
				<ul class="breadcumb">
					<li class="active"><a href="#">Home</a></li>
					<li> <span class="ion-ios-arrow-right"></span> movie listing</li>
				</ul> -->
			</div>
		</div>
	</div>
</div>

<div class="page-single movie-single movie_single">
	<div class="container">
		<div class="row ipad-width2">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="movie-img sticky-sb">
					<img src="{{ url('uploads/thumbnail').'/'.$dataFilm->thumbnail }}" alt="">
					<div class="movie-btn">
						<div class="btn-transform transform-vertical red">
							<div><a href="#" class="item item-1 redbtn"> <i class="ion-play"></i> Watch Trailer</a></div>
							<div><a href="{{$dataFilm->youtube}}" class="item item-2 redbtn fancybox-media hvr-grow"><i class="ion-play"></i></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="movie-single-ct main-content">
					<h1 class="bd-hd">{{$dataFilm->title}} <span>{{date('Y',strtotime($dataFilm->release_date))}}</span></h1>
					<div class="social-btn">
						<a href="#" class="parent-btn"><i class="ion-heart"></i> Add to Favorite</a>
						<div class="hover-bnt">
							<a href="#" class="parent-btn"><i class="ion-android-share-alt"></i>share</a>
							<div class="hvr-item">
								<a href="#" class="hvr-grow"><i class="ion-social-facebook"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-twitter"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-googleplus"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-youtube"></i></a>
							</div>
						</div>
					</div>

					<div class="movie-tabs">
						<div class="tabs">
							<ul class="tab-links tabs-mv">
								<li class="active"><a href="#overview">Overview</a></li>
								<li><a href="#reviews"> Reviews</a></li>
							</ul>
						    <div class="tab-content">
						        <div id="overview" class="tab active">
						            <div class="row">
						            	<div class="col-md-8 col-sm-12 col-xs-12">
						            		<p>{{$dataFilm->synopsis}}</p>







											<div class="title-hd-sm">
												<h4>User reviews</h4>
												<a href="#" class="time">See All {{count($reviews)}} Reviews <i class="ion-ios-arrow-right"></i></a>
											</div>
											<!-- movie user review -->

                      @foreach($reviews as $value)
                      <div class="mv-user-review-item">
                        <p class="time">
                          {{date('d M Y',strtotime($value->created_at))}} by <a href="#"> anonymous</a>
                        </p>
                        <p>{{$value->message}}</p>
                      </div>
                      <hr>
                      @endforeach



						            	</div>
						            	<div class="col-md-4 col-xs-12 col-sm-12">
						            		<div class="sb-it">
						            			<h6>Director: </h6>
						            			<p><a href="#">{{$dataFilm->director}}</a></p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Genres:</h6>
						            			<p><a href="#">{{$dataFilm->genre}}</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Release Date:</h6>
						            			<p>{{date('Y',strtotime($dataFilm->release_date))}}</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Run Time:</h6>
						            			<p>{{$dataFilm->run_time}}</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>MMPA Rating:</h6>
						            			<p>{{$dataFilm->rating}}</p>
						            		</div>

						            	</div>
						            </div>
						        </div>
						        <div id="reviews" class="tab review">
						           <div class="row">
						            	<div class="topbar-filter">
											<p>Found <span>{{count($reviews)}} reviews</span> in total</p>
											<label>Filter by:</label>
											<select>
												<option value="popularity">Popularity Descending</option>
												<option value="popularity">Popularity Ascending</option>
												<option value="rating">Rating Descending</option>
												<option value="rating">Rating Ascending</option>
												<option value="date">Release date Descending</option>
												<option value="date">Release date Ascending</option>
											</select>
										</div>


										@foreach($reviews as $value)
                      <div class="mv-user-review-item last">
  											<div class="user-infor">
                          <div>
                          	<p class="time">
  														{{date('d M Y',strtotime($value->created_at))}} by {{$value->name}}
  													</p>
  												</div>
  											</div>
  											<p>
                          {{$value->message}}
                        </p>
                    	</div>
                    @endforeach


										<div class="topbar-filter">
											<label>Write a review:</label>

										</div>

                    <div class="form-group">
                      <label for="">Name</label>
                      <input type="text" name="reviewName" class="inputreview form-control" placeholder="Your Name" value="">
                    </div>
                    <div class="form-group">
                      <label for="">Review</label>
                      <textarea name="reviewMessage" class="inputreview form-control" placeholder="Write a review" rows="8" cols="80"></textarea>
                    </div>
<br>
                    <div class="row">
                      <div class="col-md-12">
                        <button type="button" class="btn btn-submit-review" name="button">Submit Review</button>
                      </div>
                    </div>

						            </div>
						        </div>




						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('frontend/layout/footer')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">

  $(document).on('click','.inputreview',function(e){
    e.preventDefault();
  });

  $(document).on('click','.btn-submit-review',function(e){
    e.preventDefault();

    swal({
      title: "Confirmation",
      text: "are you sure to submit the review?",
      icon: "info",
      buttons: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.post('{{ url("submitReview") }}',{"_token": "{{ csrf_token() }}","film_id":"{{$dataFilm->id}}","name":$('[name="reviewName"]').val(),"message" : $('[name="reviewMessage"]').val()},function(data){
          swal('Success','Review has been submited','success').then(location.reload());
        });
      }
    });

  });
</script>

@include('frontend/layout/header')


<div class="hero common-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1> movie listing - list</h1>
					<ul class="breadcumb">
						<li class="active"><a href="#">Home</a></li>
						<li> <span class="ion-ios-arrow-right"></span> movie listing</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<form class="form_filter" action="{{ url('allfilm') }}" method="GET">
<div class="page-single movie_list">
	<div class="container">


		<div class="row ipad-width2">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="topbar-filter">
					<p>Found <span>{{count($dataFilm)}} movies</span> in total</p>
					<label>Sort by:</label>
					<select name="sort">
						<option value="desc">Release date Descending</option>
						<option value="asc">Release date Ascending</option>
					</select>
				</div>

        @foreach($dataFilm as $value)
				<div class="movie-item-style-2">
					<img src="{{ url('uploads/thumbnail').'/'.$value->thumbnail }}" alt="">
					<div class="mv-item-infor">
						<h6><a href="moviesingle.html">{{$value->title}} <span>({{date('Y',strtotime($value->release_date))}})</span></a></h6>
						<p class="describe">{{$value->synopsis}}</p>
						<p class="run-time"> Run Time: {{$value->run_time}}’    .     <span>MMPA: {{$value->rating}} </span>    .     <span>Release: {{date('d M Y',strtotime($value->release_date))}}</span></p>
						<p>Director: <a href="#">{{$value->director}}</a></p>
					</div>
				</div>
        @endforeach

			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="sidebar">
					<div class="searh-form">
						<h4 class="sb-title">Search for movie</h4>
						<form class="form-style-1" action="#">
							<div class="row">
								<div class="col-md-12 form-it">
									<label>Movie name</label>
									<input type="text" name="keywords" value="{{ (!empty($_GET['keywords']) ? $_GET['keywords'] : null) }}" placeholder="Enter keywords">
								</div>
								<div class="col-md-12 form-it">
									<label>Genres & Subgenres</label>
									<div class="group-ip">
										<select name="genre">
											<option value="">Choose Genre</option>
											<option>Science Fiction</option>
											<option>Musical Drama</option>
											<option>Drama</option>
											<option>Thriller</option>
											<option>Romantic</option>
											<option>Comedy</option>
										</select>
									</div>

								</div>
								<div class="col-md-12 form-it">
									<label>Rating Range</label>

									 <select name="rating">
										 <option value="">Choose Rating</option>
										<option>G</option>
										<option>PG</option>
										<option>PG-13</option>
										<option>R</option>
										<option>NC-17</option>
									</select>

								</div>
								<div class="col-md-12 form-it">
									<label>Release Year</label>
									<div class="row">
										<div class="col-md-6">
											<select name="year_from">
												<option value="">From</option>
												@for($i = 2013; $i < 2020;$i++)
													<option>{{$i}}</option>
												@endfor
											</select>
										</div>
										<div class="col-md-6">
											<select name="year_to">
												<option value="">To</option>
												@for($i = 2013; $i < 2020;$i++)
													<option>{{$i}}</option>
												@endfor
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-12 ">
									<input class="submit" type="submit" value="submit">
								</div>
							</div>
						</form>
					</div>


				</div>
			</div>
		</div>

	</div>
</div>
</form>

@include('frontend/layout/footer')

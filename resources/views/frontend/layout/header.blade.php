<!DOCTYPE html>

<html lang="en" class="no-js">

<head>
	<!-- Basic need -->
	<title>BlockBuster - Lambda Sangkala - Tes Hamura</title>

  <link rel="shortcut icon" href="{{asset('assets/')}}/images/logo.png">
	<meta charset="UTF-8">
	<meta name="description" content="Tes Hamura">
	<meta name="author" content="Lambda Sangkala Murbawisesa">
	<link rel="profile" href="#">

    <!--Google Font-->
    <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
	<!-- Mobile specific meta -->
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone-no">

	<!-- CSS files -->
	<link rel="stylesheet" href="{{ asset('assets') }}/css/plugins.css">
	<link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">

</head>
<body>
<!--preloading-->
<div id="preloader">
    <img class="logo" src="{{ asset('assets') }}/images/logo1.png" alt="" width="119" height="58">
    <div id="status">
        <span></span>
        <span></span>
    </div>
</div>
<!--end of preloading-->



<!-- BEGIN | Header -->
<header class="ht-header full-width-hd" id="header-light">
		<div class="row">
			<nav id="mainNav" class="navbar navbar-default navbar-custom">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header logo">
				    <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					    <span class="sr-only">Toggle navigation</span>
					    <div id="nav-icon1">
							<span></span>
							<span></span>
							<span></span>
						</div>
				    </div>
				    <a href="{{url('/')}}"><img class="logo" src="{{ asset('assets') }}/images/logo2.png" alt="" width="119" height="58"></a>
			    </div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav flex-child-menu menu-left">

					</ul>
					<ul class="nav navbar-nav flex-child-menu menu-right">
						<li class="dropdown first">
							<a class="btn btn-default lv1" href="{{url('/')}}">Home</a>
						</li>
						<li class="dropdown first">
							<a class="btn btn-default lv1" href="{{url('/allfilm')}}">List Film</a>
						</li>
					</ul>
				</div>
			<!-- /.navbar-collapse -->
	    </nav>
	    <!-- search form -->
		</div>

</header>
<!-- END | Header -->

@include('frontend/layout/header')
<div class="slider sliderv2">
	<div class="container">
		<div class="row">
	    	<div class="slider-single-item">

					@for($i = 0; $i <= 5;$i++)
						@if(!empty($dataFilm[$i]))
						<div class="movie-item">
							<div class="row">
								<div class="col-md-8 col-sm-12 col-xs-12">
									<div class="title-in">
										<div class="cate">
											<span class="blue"><a href="#">{{$dataFilm[$i]->genre}}</a></span>
										</div>
										<h1><a href="{{ url('detail').'/'.$dataFilm[$i]->id }}">{{$dataFilm[$i]->title}}<br>
									<span>{{date('Y',strtotime($dataFilm[$i]->release_date))}}</span></a></h1>
										<div class="mv-details">
											<!-- <p><i class="ion-android-star"></i><span>7.4</span> /10</p> -->
											<ul class="mv-infor">
												<li>  Run Time: {{$dataFilm[$i]->run_time}} </li>
												<li>  Rated: {{$dataFilm[$i]->rating}}  </li>
												<li>  Release: {{date('d M Y',strtotime($dataFilm[$i]->release_date))}}</li>
											</ul>
										</div>
										<div class="btn-transform transform-vertical">
										<div><a href="{{ url('detail').'/'.$dataFilm[$i]->id }}" class="item item-1 redbtn">more detail</a></div>
										<div><a href= "#" class="item item-2 redbtn hvrbtn">more detail</a></div>
									</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<div class="mv-img-2">
										<a href="{{ url('detail').'/'.$dataFilm[$i]->id }}"><img src="{{ url('uploads/thumbnail').'/'.$dataFilm[$i]->thumbnail }}" alt=""></a>
									</div>
								</div>
							</div>
						</div>
						@endif
					@endfor



	    	</div>
	    </div>
	</div>
</div>
		<div class="buster-light">
<div class="movie-items  full-width">
	<div class="row">
		<div class="col-md-12">
			<div class="title-hd">
				<h2>List Film</h2>
				<a href="{{ url('allfilm') }}" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>
			</div>
			<div class="tabs">

			    <div class="tab-content">
			        <div id="tab1-h2" class="tab active">
			            <div class="row">
			            	<div class="slick-multiItem2">


											@foreach($dataFilm as $value)
												<div class="slide-it">
													<div class="movie-item">
														<div class="mv-img">
															<img src="{{ url('uploads/thumbnail').'/'.$value->thumbnail }}" alt="">
														</div>
														<div class="hvr-inner">
															<a  href="{{url('detail').'/'.$value->id}}"> Read more <i class="ion-android-arrow-dropright"></i> </a>
														</div>
														<div class="title-in">
															<h6><a href="#">{{$value->title}}</a></h6>
															<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
														</div>
													</div>
												</div>
											@endforeach




			            	</div>
			            </div>
			        </div>



			    </div>
			</div>




		</div>
	</div>
</div>

		</div>

@include('frontend/layout/footer')
